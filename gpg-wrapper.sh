#!/bin/bash

VAULT_PW_FILENAME="vault"
gpg --quiet --batch --use-agent --decrypt $VAULT_PW_FILENAME
