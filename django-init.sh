set -x

export DJANGO_SECRET_KEY="{{ django_secret_key }}"
cd {{ base_dir }}/site
python3 -m venv $VIRTUAL_ENV
source venv/bin/activate
pip install -r requirements.txt
python3 manage.py collectstatic --noinput
python3 manage.py migrate --noinput
python3 manage.py createsuperuser --noinput || echo "createsuperuser exited with error (as expected)"
PYTHONPATH=./
./manage.py makemessages --all --build-all --ignore="venv/*"
./manage.py compilemessages
mkdir -p media
chown -R www-data:www-data {{ base_dir }}

if ! grep -q DJANGO_SECRET_KEY /etc/apache2/envvars; then
  echo 'export DJANGO_SECRET_KEY="{{ django_secret_key }}"' >> /etc/apache2/envvars
fi

service apache2 restart
service memcached restart
service ssh restart
