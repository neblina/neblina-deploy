FROM debian:bullseye

COPY django-init.sh /django-init.sh
RUN chmod +x /django-init.sh

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN apt update
RUN apt -y install openssh-server python3.9 python3-venv gettext
RUN apt -y install libapache2-mod-wsgi-py3 memcached apache2
RUN apt -y install python3-certbot python3-certbot-apache cron
RUN a2enmod wsgi

ENV VIRTUAL_ENV={{ base_dir }}/site/venv
ENV DJANGO_SUPERUSER_PASSWORD={{ django_admin_pwd }}
ENV DJANGO_SUPERUSER_USERNAME=neblina
ENV DJANGO_SUPERUSER_EMAIL=neblinaxyz@riseup.net

ENTRYPOINT /bin/bash -c /django-init.sh && sh

